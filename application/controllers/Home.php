<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->load->model('article/Article_model');

		$data['articles'] = $this->Article_model->get(array('limit' => 3));
		$this->load->view('frontend/home', $data);
	}
}
