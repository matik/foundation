<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Api controllers class
 *
 * @package     SYSCMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Sistiandy Syahbana nugraha <sistiandy.web.id>
 */
class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $res = array('message' => 'Nothing here');

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($res));
    }
    
    public function getCategories($id = NULL) {
        $this->load->model('product/Product_model');
        $res = $this->Product_model->get_category();

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($res));
    }

}
