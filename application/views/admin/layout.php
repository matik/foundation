<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SYSCMS <?php echo isset($title) ? ' | ' . $title : null; ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?php echo media_url('ico/favicon.jpg'); ?>" type="image/x-icon">
  <link rel="stylesheet" href="<?php echo media_url() ?>/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo media_url() ?>/css/jquery-ui.min.css">
  <link rel="stylesheet" href="<?php echo media_url() ?>/css/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="<?php echo media_url() ?>/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo media_url() ?>/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo media_url() ?>/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo media_url() ?>/css/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo media_url() ?>/css/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo media_url() ?>/css/jquery.notyfy.css">
  
  <script src="<?php echo media_url() ?>/js/jquery-2.2.3.min.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini" <?php echo isset($ngapp) ? $ngapp : null; ?>>
      <div class="wrapper">

        <header class="main-header">
          <a href="index2.html" class="logo">
            <span class="logo-mini"><b>S</b>YS</span>
            <span class="logo-lg"><b>Sys</b>CMS</span>
          </a>
          <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
              <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?php echo media_url() ?>/img/user.png" class="user-image" alt="User Image">
                    <span class="hidden-xs"><?php echo ucfirst($this->session->userdata('ufullname')); ?></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="user-header">
                      <img src="<?php echo media_url() ?>/img/user.png" class="img-circle" alt="User Image">

                      <p>
                        <?php echo ucfirst($this->session->userdata('ufullname')); ?> - <?php echo ucfirst($this->session->userdata('urolename')); ?>
                        <small><?php echo $this->session->userdata('uemail'); ?></small>
                      </p>
                    </li>
                    <li class="user-footer">
                      <div class="pull-left">
                        <a href="<?php echo site_url('admin/profile') ?>" class="btn btn-default btn-flat">Profile</a>
                      </div>
                      <div class="pull-right">
                        <a href="<?php echo site_url('admin/auth/logout?location=' . htmlspecialchars($_SERVER['REQUEST_URI'])) ?>" class="btn btn-default btn-flat">Sign out</a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </nav>
        </header>
        <?php $this->load->view('admin/sidebar'); ?>

        <?php isset($main) ? $this->load->view($main) : null; ?>
        <div id="deleteModal"></div>
        <footer class="main-footer">
          <div class="pull-right hidden-xs">
            <b>Version</b> 0.1.0
          </div>
          <strong>Copyright &copy; <a href="http://matik.id">Matik Creative Technology</a>.</strong> All rights
          reserved.
        </div>

        <script src="<?php echo media_url() ?>/js/jquery-ui.min.js"></script>
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <script src="<?php echo media_url() ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo media_url() ?>/js/tinymce/tinymce.min.js"></script>
        <script src="<?php echo media_url() ?>/js/app.min.js"></script>
        <script src="<?php echo media_url() ?>/js/jquery.notyfy.js"></script>
        <script src="<?php echo media_url() ?>/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo media_url() ?>/js/dataTables.bootstrap.min.js"></script>

        <script type="text/javascript">

          function doDelete(id, title, module) {
            var placementId = "deleteModal";
            html = '<div id="modalWindow" class="modal fade" style="display:none;">';
            html += '<div class="modal-dialog" role="document">';
            html += '<div class="modal-content">';
            html += '<form action="<?php echo site_url() ?>admin/'+module+'/delete/'+id+'" method="post" role="form">';
            html += '<div class="modal-header">';
            html += '<a class="close" data-dismiss="modal">×</a>';
            html += '<h4>Konfirmasi Penghapusan Data</h4>';
            html += '</div>';
            html += '<div class="modal-body">';
            html += '<h5> Apakah anda yakin akan menghapus data ini? </h5>';
            html += '<input type="hidden" name="delName" value="'+title+'">';
            html += '</div>';
            html += '<div class="modal-footer">';
            html += '<button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>';
            html += '<button onclick="startLoading()" type="submit" class="btn btn-danger">Hapus</button>';

      html += '</div>'; // footer
      html += '</form>'; // footer

      html += '</div>';
      html += '</div>';
      html += '</div>'; // modalWindow

      $("#" + placementId).html(html);
      $("#modalWindow").modal();

      //GC Destroy modal on close
      $('#' + placementId).on('hidden', function() {
        $(this).data('modal', null);
      });

    }
  </script>

  <script>
    var loadFile = function(event) {
      var output = document.getElementById('outputImg');
      output.src = URL.createObjectURL(event.target.files[0]);
    };
  </script>

  <script>
            //Initiation dataTable
            $(function () {
              $('.table-init').DataTable({
                "processing": true,
                "serverSide": true,
                "aaSorting": [],
                "ajax": {
                  "url": "<?php echo current_url().'/ajax_list' ?>",
                  "type": "POST"
                },
                "oLanguage": {
                  "sSearch": "Pencarian :"
                },
                "aoColumnDefs": [
                {
                  'bSortable': false,
                  'aTargets': [-1]
                        } //disables sorting for last column
                        ],
                        "sPaginationType": "full_numbers",
                      });
            });
          </script>
          <script>

            //Initiation datepicker
            $(function () {
              $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '-55:+10',
                dateFormat: "yy-mm-dd",
              });
            });
          </script>

          <script>
            $(function () {
             tinymce.init({
              selector: '.editor',
              branding: false,
              height: 500,
              menubar: false,
              plugins: [
              'advlist autolink lists link image charmap print preview anchor textcolor',
              'searchreplace visualblocks code fullscreen',
              'insertdatetime media table contextmenu paste code help wordcount'
              ],
              toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            });
           });
         </script>

         <?php if ($this->session->flashdata('success')) { ?>
         <script>
          $(function () {
            notyfy({
              layout: 'top',
              type: 'success',
              showEffect: function (bar) {
                bar.animate({height: 'toggle'}, 500, 'swing');
              },
              hideEffect: function (bar) {
                bar.animate({height: 'toggle'}, 500, 'swing');
              },
              timeout: 3000,
              text: '<?php echo $this->session->flashdata('success') ?>'
            });
          });
        </script>
        <?php } ?>
      </body>
      </html>
