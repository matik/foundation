<div id="team" class="section md-padding">

	<!-- Container -->
	<div class="container">

		<!-- Row -->
		<div class="row">

			<!-- Section header -->
			<div class="section-header text-center">
				<h2 class="title">Tim Kami</h2>
			</div>
			<!-- /Section header -->
		</div>

			<!-- team -->
			<div class="boxes">
				
			
			<div class="box-container col-sm-4">
				<div class="team">
					<div class="team-img">
						<img class="img-responsive" src="<?php echo media_url() ?>img/team1.jpg" alt="">
						<div class="overlay">
							<div class="team-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
							</div>
						</div>
					</div>
					<div class="team-content">
						<h3>Wahyu Hidayat</h3>
						<span>CEO Funder</span>
					</div>
				</div>
			</div>
			<!-- /team -->

			<!-- team -->
			<div class="box-container col-sm-4">
				<div class="team">
					<div class="team-img">
						<img class="img-responsive" src="<?php echo media_url() ?>img/team2.jpg" alt="">
						<div class="overlay">
							<div class="team-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
							</div>
						</div>
					</div>
					<div class="team-content">
						<h3>Wahyu Adi</h3>
						<span>CEO</span>
					</div>
				</div>
			</div>
			<!-- /team -->
			</div>

		</div>
		<!-- /Row -->

	</div>
	<!-- /Container -->

</div>