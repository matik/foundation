<div id="testimonial" class="section md-padding">

	<!-- Background Image -->
	<div class="bg-img" style="background-image: url('<?php echo media_url() ?>img/testimoni.jpeg');">
		<div class="overlay"></div>
	</div>
	<!-- /Background Image -->

	<!-- Container -->
	<div class="container">

		<!-- Row -->
		<div class="row">

			<!-- Testimonial slider -->
			<div class="col-md-10 col-md-offset-1">
				<div id="testimonial-slider" class="owl-carousel owl-theme">

					<!-- testimonial -->
					<div class="testimonial">
						<div class="testimonial-meta">
							<img src="<?php echo media_url() ?>img/perso1.jpg" alt="">
							<h3 class="white-text">Haikal Ramadhan</h3>
							<span>Kepala Sekolah SMK</span>
						</div>
						<p class="white-text">Suaranya bagus, Kualitas Alatnya bagus, Pelayanannya memuasakan. Teamnya pun ramah, Engga Salah Deh Nyewa Di WMF SoundSystem.</p>

					</div>
					<!-- /testimonial -->

					<!-- testimonial -->
					<div class="testimonial">
						<div class="testimonial-meta">
							<img src="<?php echo media_url() ?>img/perso2.jpg" alt="">
							<h3 class="white-text">Sistiandy Syahbana Putra</h3>
							<span>Santri Majelis Ta'lim</span>
						</div>
						<p class="white-text">Alhamdulillah ketemu WMF SoundSystem, Tadinya bingung nyari dimana. Harga
						murah dan terjangkau, pelayanannya pun oke. Terimakasih WMF SoundSystem</p>
					</div>
					<!-- /testimonial -->

				</div>
			</div>
			<!-- /Testimonial slider -->

		</div>
		<!-- /Row -->

	</div>
	<!-- /Container -->

</div>