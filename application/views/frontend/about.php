<div id="about" class="section md-padding">

	<!-- Container -->
	<div class="container">

		<!-- Row -->
		<div class="row">

			<!-- Section header -->
			<div class="section-header text-center">
				<h2 class="title">Mengapa Memilih kami?</h2>
			</div>
			<!-- /Section header -->

			<!-- about -->
			<div class="col-md-4">
				<div class="about">
					<i class="fa fa-cogs"></i>
					<h3>Tenaga Ahli</h3>
					<p>Kami selalu mengedepankan kepuasan pelanggan maka dari itu teknisi kami pun akan memberikan pelayanan terbaik</p>
				</div>
			</div>
			<!-- /about -->

			<!-- about -->
			<div class="col-md-4">
				<div class="about">
					<i class="fa fa-magic"></i>
					<h3>Kualitas Terbaik</h3>
					<p>Alat yang kami sewakan pun berkualitas terbaik,terawat membuat kami selalu memenuhi kebutuhan pelanggan</p>
				</div>
			</div>
			<!-- /about -->

			<!-- about -->
			<div class="col-md-4">
				<div class="about">
					<i class="fa fa-volume-up"></i>
					<h3>Suara Jernih</h3>
					<p>Kami memiliki alat dengan kualitas terbaik, sudah pasti mmenghasilkan suara yang jernih.</p>
				</div>
			</div>
			<!-- /about -->

		</div>
		<!-- /Row -->

	</div>
	<!-- /Container -->

</div>