<div id="features" class="section md-padding bg-grey">

	<!-- Container -->
	<div class="container">

		<!-- Row -->
		<div class="row">

			<!-- why choose us content -->
			<div class="col-md-6">
				<div class="section-header">
					<h2 class="title">Tentang Kami</h2>
				</div>
				<p>Kami menyediakan berbagai macam paket penyewaan sound system yang dapat menyesuaikan kebutuhan acara anda. Paket sewa sound system tersebut dapat digunakan untuk indoor, outdoor dan juga panggung.</p>
			</div>
			<!-- /why choose us content -->

			<!-- About slider -->
			<div class="col-md-6">
				<div id="about-slider" class="owl-carousel owl-theme">
					<img class="img-responsive" src="<?php echo media_url() ?>img/about1.jpg" alt="">
					<img class="img-responsive" src="<?php echo media_url() ?>img/about2.jpg" alt="">
					<img class="img-responsive" src="<?php echo media_url() ?>img/about3.jpg" alt="">
					<img class="img-responsive" src="<?php echo media_url() ?>img/about4.jpg" alt="">
				</div>
			</div>
			<!-- /About slider -->

		</div>
		<!-- /Row -->

	</div>
	<!-- /Container -->

</div>