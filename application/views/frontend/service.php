<div id="service" class="section md-padding">

	<!-- Container -->
	<div class="container">

		<!-- Row -->
		<div class="row">

			<!-- Section header -->
			<div class="section-header text-center">
				<h2 class="title">Layanan Kami</h2>
			</div>
			<!-- /Section header -->

			<!-- service -->
			<div class="col-md-4 col-sm-6">
				<div class="service">
					<i class="fa fa-diamond"></i>
					<h3>Acara Pernikahan</h3>
					<p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
				</div>
			</div>
			<!-- /service -->

			<!-- service -->
			<div class="col-md-4 col-sm-6">
				<div class="service">
					<i class="fa fa-rocket"></i>
					<h3>Acara Khitanan</h3>
					<p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
				</div>
			</div>
			<!-- /service -->

			<!-- service -->
			<div class="col-md-4 col-sm-6">
				<div class="service">
					<i class="fa fa-users"></i>
					<h3>Acara Pengajian</h3>
					<p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
				</div>
			</div>
			<!-- /service -->

			<!-- service -->
			<div class="col-md-4 col-sm-6">
				<div class="service">
					<i class="fa fa-renren"></i>
					<h3>Acara Reuni</h3>
					<p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
				</div>
			</div>
			<!-- /service -->

			<!-- service -->
			<div class="col-md-4 col-sm-6">
				<div class="service">
					<i class="fa fa-graduation-cap"></i>
					<h3>Acara Wisuda</h3>
					<p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
				</div>
			</div>
			<!-- /service -->

			<!-- service -->
			<div class="col-md-4 col-sm-6">
				<div class="service">
					<i class="fa fa-cogs"></i>
					<h3>Berbagai Acara Lainnya</h3>
					<p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
				</div>
			</div>
			<!-- /service -->

		</div>
		<!-- /Row -->

	</div>
	<!-- /Container -->

</div>