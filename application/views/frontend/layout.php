<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>WMF Sound System</title>

	<!-- Google font -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet"> -->

	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>css/owl.theme.default.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>css/magnific-popup.css" />
	<link rel="stylesheet" href="<?php echo media_url() ?>/css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>/css/style.css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

	<?php $this->load->view('frontend/header'); ?>

	<!-- Article -->
	<?php isset($main) ? $this->load->view($main) : null; ?>
	<!-- /Article -->
	<?php $this->load->view('frontend/footer'); ?>

	<div id="back-to-top"></div>

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="<?php echo media_url() ?>js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="<?php echo media_url() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo media_url() ?>js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo media_url() ?>js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="<?php echo media_url() ?>js/main.js"></script>

</body>

</html>

