
<header>

	<!-- Nav -->
		<nav id="nav" class="navbar">
			<div class="container">

				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="<?php echo site_url() ?>">
							<img class="logo" src="<?php echo media_url() ?>img/WMF.png" alt="logo">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="#home">Beranda</a></li>
					<li class="has-dropdown"><a href="#blog">Artikel</a>
						<ul class="dropdown">
							<li><a href="blog-single.html">Pemasangan Sound System Di Citayam</a></li>
						</ul>
					</li>
					<li><a href="#service">Layanan</a></li>
					<li><a href="#features">Tentang Kami</a></li>
					<li><a href="#contact">Kontak</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
		<!-- /Nav -->

	</header>