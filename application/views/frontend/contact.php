<div id="contact" class="section md-padding bg-grey">

	<!-- Container -->
	<div class="container">

		<!-- Row -->
		<div class="row">

			<!-- Section-header -->
			<div class="section-header text-center">
				<h2 class="title">Get in touch</h2>
			</div>
			<!-- /Section-header -->

			<!-- contact -->
			<div class="col-sm-4">
				<div class="contact">
					<i class="fa fa-phone"></i>
					<h3>Telepon</h3>
					<p>0881-1053-165</p>
				</div>
			</div>
			<!-- /contact -->

			<!-- contact -->
			<div class="col-sm-4">
				<div class="contact">
					<i class="fa fa-envelope"></i>
					<h3>Email</h3>
					<p>wmfsoundsystem@gmail.com</p>
				</div>
			</div>
			<!-- /contact -->

			<!-- contact -->
			<div class="col-sm-4">
				<div class="contact">
					<i class="fa fa-map-marker"></i>
					<h3>Alamat</h3>
					<p>Jl.Lio Sawah Indah Citayam RT 01/08 No. 110B Kota Depo</p>
				</div>
			</div>
			<!-- /contact -->

		</div>
		<!-- /Row -->

	</div>
	<!-- /Container -->

</div>