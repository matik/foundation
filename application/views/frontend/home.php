<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>HTML Template</title>

	<!-- Google font -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet"> -->

	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>css/owl.theme.default.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>css/magnific-popup.css" />
	<link rel="stylesheet" href="<?php echo media_url() ?>/css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="<?php echo media_url() ?>css/style.css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
<header id="home">
	<!-- Background Image -->
	<div class="bg-img" style="background-image: url('<?php echo media_url() ?>img/audio.jpeg');">
		<div class="overlay"></div>
	</div>
	<!-- /Background Image -->

	<!-- Nav -->
	<nav id="nav" class="navbar nav-transparent">
		<div class="container">

			<div class="navbar-header">
				<!-- Logo -->
				<div class="navbar-brand">
					<a href="<?php echo site_url() ?>">
						<img class="logo" src="<?php echo media_url() ?>img/WMF.png" alt="logo">
						<img class="logo-alt" src="<?php echo media_url() ?>img/WMF-alt.png" alt="logo">
					</a>
				</div>
				<!-- /Logo -->

				<!-- Collapse nav button -->
				<div class="nav-collapse">
					<span></span>
				</div>
				<!-- /Collapse nav button -->
			</div>

			<!--  Main navigation  -->
			<ul class="main-nav nav navbar-nav navbar-right">
				<li><a href="#home">Beranda</a></li>
				<li class="has-dropdown"><a href="#blog">Artikel</a>
					<ul class="dropdown">
						<li><a href="blog-single.html">Penyewaaan Sound System Di Citayam</a></li>
					</ul>
				</li>
				<li><a href="#service">Layanan</a></li>
				<li><a href="#features">Tentang Kami</a></li>
				<li><a href="#team">Team Kami</a></li>
				<li><a href="#contact">Kontak</a></li>
			</ul>
			<!-- /Main navigation -->

		</div>
	</nav>
	<!-- /Nav -->

	<!-- home wrapper -->
	<div class="home-wrapper">
		<div class="container">
			<div class="row">

				<!-- home content -->
				<div class="col-md-10 col-md-offset-1">
					<div class="home-content">
						<p class="white-text head-text">Jasa Pemasangan Sound System</h1>
							<p class="white-text">WMF SoundSystem adalah Solusi Tepat untuk anda yang membutuhkan keperluan sound baik indoor maupun outdoor, Hubungi Kami Kapan saja.
							</p>
						</div>
					</div>
					<!-- /home content -->

				</div>
			</div>
		</div>
		<!-- /home wrapper -->

	</header>
	
	<?php $this->load->view('frontend/about'); ?>

	<!-- Article -->
	<?php $this->load->view('frontend/article/main.php'); ?>
	<!-- /Article -->

	<?php $this->load->view('frontend/service'); ?>
	<?php $this->load->view('frontend/features'); ?>
	<?php $this->load->view('frontend/testimonial'); ?>
	<?php $this->load->view('frontend/team'); ?>
	<?php $this->load->view('frontend/contact'); ?>
	<?php $this->load->view('frontend/footer'); ?>

	<div id="back-to-top"></div>

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="<?php echo media_url() ?>js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="<?php echo media_url() ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo media_url() ?>js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo media_url() ?>js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="<?php echo media_url() ?>js/main.js"></script>

</body>

</html>
