<div id="blog" class="section md-padding bg-grey">

	<!-- Container -->
	<div class="container">

		<!-- Row -->
		<div class="row">

			<!-- Section header -->
			<div class="section-header text-center">
				<h2 class="title">Artikel Terakhir</h2>
			</div>
			<!-- /Section header -->

			<?php foreach ($articles as $row):?>
			<div class="col-md-4">
				<div class="blog">
					<div class="blog-img">
						<img class="img-responsive" src="<?php echo upload_url($row['article_image']) ?>" alt="">
					</div>
					<div class="blog-content">
						<ul class="blog-meta">
							<li><i class="fa fa-clock-o"></i><?php echo pretty_date($row['article_input_date'], 'l, d m Y', false) ?></li>
						</ul>
						<h3><?php echo $row['article_name'] ?></h3>
						<p><?php echo $row['article_description'] ?></p>
						<a href="<?php echo article_url($row) ?>">Selengkapnya</a>
					</div>
				</div>
			</div>
			<?php endforeach; ?>

		</div>
		<!-- /Row -->

	</div>
	<!-- /Container -->

</div>