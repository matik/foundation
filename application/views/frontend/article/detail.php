<div class="header-wrapper sm-padding bg-grey">
	<div class="container">
		<h2>Artikel</h2>
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo site_url() ?>">Home</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url() ?>">Article</a></li>
			<li class="breadcrumb-item active"><?php echo $article['article_name'] ?></li>
		</ul>
	</div>
</div>

<div id="blog" class="section md-padding">

	<!-- Container -->
	<div class="container">

		<!-- Row -->
		<div class="row">

			<!-- Main -->
			<main id="main" class="col-md-9">
				<div class="blog">
					<div class="blog-img">
						<img class="img-responsive" src="<?php echo upload_url($article['article_image']) ?>" alt="">
					</div>
					<div class="blog-content">
						<ul class="blog-meta">
							<li><i class="fa fa-clock-o"></i><?php echo pretty_date($article['article_input_date'], 'l, d m Y', false) ?></li>
						</ul>
						<h3><?php echo $article['article_name'] ?></h3>
						<?php echo $article['article_specification'] ?>
					</div>
				</div>
			</main>
			<!-- /Main -->


			<!-- Aside -->
			<aside id="aside" class="col-md-3">

				<!-- Search -->
				<div class="widget">
					<div class="widget-search">
						<input class="search-input" type="text" placeholder="search">
						<button class="search-btn" type="button"><i class="fa fa-search"></i></button>
					</div>
				</div>
				<!-- /Search -->

				<!-- Posts sidebar -->
				<div class="widget">
					<h3 class="title">Populare Posts</h3>

					<?php foreach ($populare as $row): ?>
					<div class="widget-post">
						<a href="<?php echo article_url($row) ?>">
							<div class="img-responsive">
							<img src="<?php echo upload_url($row['article_image']) ?>" alt="" width="270"> 
							</div>
							<?php echo $row['article_name'] ?>
						</a>
						<ul class="blog-meta">
							<li><?php echo pretty_date($row['article_input_date'], 'l, d m Y', false) ?></li>
						</ul>
					</div>
					<?php endforeach; ?>
				</div>
				<!-- /Posts sidebar -->

			</aside>
			<!-- /Aside -->

		</div>
		<!-- /Row -->

	</div>
	<!-- /Container -->

</div>