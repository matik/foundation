<?php
if (isset($article)) {
    $article_id = $article['article_id'];
    $article_name = $article['article_name'];
    $article_description = $article['article_description'];
    $article_specification = $article['article_specification'];
    $article_image = $article['article_image'];
} else {
    $article_name = set_value('article_name');
    $article_description = set_value('article_description');
    $article_specification = set_value('article_specification');
    $article_image = set_value('article_image');
}
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Artikel
            <small><?php echo $operation; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Artikel</li>
            <li class="active"><?php echo $operation; ?></li>
        </ol>
    </section>

    <section class="content">
        <?php echo form_open_multipart(current_url()); ?>
        <div class="row">
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-body">
                        <?php echo validation_errors(); ?>
                        <?php if (isset($article)) { ?>
                        <input type="hidden" name="article_id" value="<?php echo $article_id; ?>">
                        <?php } ?>

                        <div class="form-group">
                            <label>Nama Artikel <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                            <input name="article_name" required="" type="text" class="form-control" value="<?php echo $article_name ?>" placeholder="Nama produk">
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" placeholder="Deskripsi" name="article_description" placeholder="Deskripsi"> <?php echo $article_description ?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Spesifikasi</label>
                            <textarea class="form-control editor" placeholder="Spesifikasi" name="article_specification"> <?php echo $article_specification ?></textarea>
                        </div>   

                        <p class="text-muted">*) Kolom wajib diisi.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Gambar utama</label>
                            <input type="file" name="article_image" accept="image/*" onchange="loadFile(event)">
                        </div>
                        <?php if (isset($article) AND $article_image != null) { ?>
                        <img src="<?php echo upload_url($article_image) ?>" class="img-responsive img-thumbnail"/>
                        <?php }else{ ?>
                        <img class="img-responsive" id="outputImg"/>
                        <?php } ?>
                        <button type="submit" class="btn btn-flat btn-block btn-success"><span class="fa fa-check-circle"></span> Simpan</button>
                        <a href="<?php echo site_url('admin/article'); ?>" class="btn btn-flat btn-block btn-info"><span class="fa fa-arrow-circle-left"></span> Batal</a>
                        <?php if (isset($article)) { ?>
                        <a href="#" onclick="doDelete(<?php echo $article['article_id'] ?>, '<?php echo $article['article_name'] ?>', 'article')" class="btn btn-flat btn-block btn-danger" ><span class="fa fa-close"></span> Hapus</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </section>
</div>