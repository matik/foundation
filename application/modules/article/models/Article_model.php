<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * user Model Class
 *
 * @package     SYSCMS
 * @subpackage  Models
 * @category    Models
 * @author      Sistiandy Syahbana nugraha <sistiandy.web.id>
 */
class Article_model extends CI_Model {

    var $table = 'articles';
    var $all_column = array(
        'articles.article_id', 
        'article_name', 
        'article_description', 
        'article_specification',
        'article_image', 
        'article_is_deleted',
        'article_input_date',
        'article_last_update'); //set all column field database
    var $order = array('article_last_update' => 'desc'); // default order 

    function __construct() {
        parent::__construct();
    }

    private function _get_datatables_query() {

        $this->db->from($this->table);
        $this->db->where('article_is_deleted', FALSE);

        $i = 0;

        foreach ($this->all_column as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->all_column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->all_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $this->db->select($this->all_column);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Get From Databases
    function get($params = array()) {
        if (isset($params['article_id'])) {
            $this->db->where('articles.article_id', $params['article_id']);
        }
        $this->db->where('article_is_deleted', FALSE);

        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } else {
            $this->db->order_by('article_last_update', 'desc');
        }
        $this->db->select($this->all_column);
        $res = $this->db->get('articles');

        if (isset($params['article_id'])) {
            return $res->row_array();
        } else {
            return $res->result_array();
        }
    }

    // Insert some data to table
    function add($data = array()) {

        if (isset($data['article_id'])) {
            $this->db->set('article_id', $data['article_id']);
        }

        if (isset($data['article_name'])) {
            $this->db->set('article_name', $data['article_name']);
        }

        if (isset($data['article_description'])) {
            $this->db->set('article_description', $data['article_description']);
        }

        if (isset($data['article_specification'])) {
            $this->db->set('article_specification', $data['article_specification']);
        }

        if (isset($data['article_image'])) {
            $this->db->set('article_image', $data['article_image']);
        }

        if (isset($data['article_is_deleted'])) {
            $this->db->set('article_is_deleted', $data['article_is_deleted']);
        }

        if (isset($data['article_input_date'])) {
            $this->db->set('article_input_date', $data['article_input_date']);
        }

        if (isset($data['article_last_update'])) {
            $this->db->set('article_last_update', $data['article_last_update']);
        }

        if (isset($data['article_id'])) {
            $this->db->where('article_id', $data['article_id']);
            $this->db->update('articles');
            $id = $data['article_id'];
        } else {
            $this->db->insert('articles');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

    // Drop some data to table
    function delete($id) {
        $this->db->set('article_is_deleted', 1);
        $this->db->where('article_id', $id);
        $this->db->update('articles');
    }

    // Get Image From Databases
    function get_image($params = array()) {
        $this->db->select('article_images.image_id, image_value');

        if (isset($params['article_id'])) {
            $this->db->where('article_images.articles_article_id', $params['article_id']);
        }

        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } else {
            $this->db->order_by('article_images.image_id', 'desc');
        }
        $this->db->select('image_id, image_value');

        $res = $this->db->get('article_images');

        if (isset($params['image_id'])) {
            return $res->row_array();
        } else {
            return $res->result_array();
        }
    }

    // Insert some data to table
    function add_image($data = array()) {

        if (isset($data['image_id'])) {
            $this->db->set('image_id', $data['image_id']);
        }

        if (isset($data['image_value'])) {
            $this->db->set('image_value', $data['image_value']);
        }

        if (isset($data['articles_article_id'])) {
            $this->db->set('articles_article_id', $data['articles_article_id']);
        }

        if (isset($data['image_id'])) {
            $this->db->where('image_id', $data['image_id']);
            $this->db->update('article_images');
            $id = $data['image_id'];
        } else {
            $this->db->insert('article_images');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

    // Drop some data to table
    function delete_image($id) {
        $this->db->where('image_id', $id);
        $this->db->delete('article_images');
    }

}
