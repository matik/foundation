<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('article/Article_model');
    }

	public function index()
	{

		$data['articles'] = $this->Article_model->get(array('limit' => 3));
		$data['main'] = 'frontend/article/main';
		$this->load->view('frontend/home', $data);
	}

	public function detail($id = null)
	{

		$article = $this->Article_model->get(array('article_id' => $id));

		if ($article == null) {
			redirect(site_url());
		}else{
			$data['article'] = $article;
		}
		
		$data['populare'] = $this->Article_model->get(array('limit' => 6));
		$data['main'] = 'frontend/article/detail';
		$this->load->view('frontend/layout', $data);
	}
}
