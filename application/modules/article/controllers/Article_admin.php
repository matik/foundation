<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Article controllers Class
 *
 * @package     SYSCMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Sistiandy Syahbana nugraha <sistiandy.web.id>
 */
class Article_admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model('article/Article_model');
    }

    public function index() {
        $data['articles'] = $this->Article_model->get();
        $data['title'] = 'Artikel';
        $data['main'] = 'article/list';
        $this->load->view('admin/layout', $data);
    }

    public function ajax_list() {
        $keys = $this->Article_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($keys as $key) {
            $no++;
            $row = array();
            $row[] = $key->article_name;

            //add html for action
            $row[] = btn_list('article', $key->article_id, $key->article_name) ;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Article_model->count_all(),
            "recordsFiltered" => $this->Article_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    // Add and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('article_name', 'Nama produk', 'trim|required|xss_clean');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button position="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('article_id')) {
                $params['article_id'] = $id;
            } else {
                $params['article_input_date'] = date('Y-m-d H:i:s');
                $params['article_is_deleted'] = 0;
            }
            $params['article_name'] = $this->input->post('article_name');
            $params['article_description'] = $this->input->post('article_description');
            $params['article_specification'] = $this->input->post('article_specification');
            $params['article_last_update'] = date('Y-m-d H:i:s');
            if (!empty($_FILES['article_image']['name'])) {
                $params['article_image'] = $this->do_upload('article_image');
            }
            $status = $this->Article_model->add($params);

            // activity log
            $this->load->model('logs/Logs_model');
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('uid'),
                    'log_module' => 'Artikel',
                    'log_action' => $data['operation'],
                    'log_info' => 'ID:' . $status . ';Name:' . $this->input->post('article_name')
                )
            );

            $this->session->set_flashdata('success', $data['operation'] . ' Artikel Berhasil');
            redirect('admin/article');
        } else {
            if ($this->input->post('article_id')) {
                redirect('admin/article/edit/' . $this->input->post('article_id'));
            }

            // Edit mode
            if (!is_null($id)) {
                $object = $this->Article_model->get(array('article_id' => $id));
                if ($object == NULL) {
                    redirect('admin/article');
                } else {
                    $data['article'] = $object;
                }
            }
            $data['title'] = $data['operation'] . ' Artikel';
            $data['main'] = 'article/add';
            $this->load->view('admin/layout', $data);
        }
    }

    // View data detail
    public function view($id = NULL) {
        $data['article'] = $this->Article_model->get(array('article_id' => $id));
        $data['title'] = 'Artikel';
        $data['main'] = 'article/view';
        $this->load->view('admin/layout', $data);
    }

    // Delete to database
    public function delete($id = NULL) {
        if ($_POST) {
            $this->Article_model->delete($id);
            // activity log
            $this->load->model('logs/Logs_model');
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('uid'),
                    'log_module' => 'Article',
                    'log_action' => 'Hapus',
                    'log_info' => 'ID:' . $id . ';Title:' . $this->input->post('delName')
                )
            );
            $this->session->set_flashdata('success', 'Hapus Artikel berhasil');
            redirect('admin/article');
        } elseif (!$_POST) {
            $this->session->set_flashdata('delete', 'Delete');
            redirect('admin/article/edit/' . $id);
        }
    }

    // Setting Upload File Requied
    function do_upload($name) {
        $config['upload_path'] = FCPATH . 'uploads/';

        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '32000';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($name)) {
            echo $config['upload_path'];
            $this->session->set_flashdata('success', $this->upload->display_errors(''));
            redirect(uri_string());
        }

        $upload_data = $this->upload->data();

        return $upload_data['file_name'];
    }

}
